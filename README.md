# Project Success Moodle Theme

This is a basic Moodle theme for use with Project Success. It is based on the *Standard* theme that ships with Moodle and is customized to reflect the look and feel of the [main Project Success website](http://kansasprojectsuccess.org/). Nothing too fancy; it mainly overrides colors and Moodle's obsession with large radii on lower corners of boxes.

## Reference

- [MoodleDocs on Themes](http://docs.moodle.org/dev/Themes_2.0)